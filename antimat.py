# -*- coding: utf-8 -*-
import re


class ObsceneCensorRus:
    letters = {
        'P': u'пПnPp',
        'I': u'иИiI1u',
        'E': u'еЕeE',
        'D': u'дДdD',
        'Z': u'зЗ3zZ3',
        'M': u'мМmM',
        'U': u'уУyYuU',
        'O': u'оОoO0',
        'L': u'лЛlL',
        'S': u'сСcCsS',
        'A': u'аАaA',
        'N': u'нНhH',
        'G': u'гГgG',
        'CH':u'чЧ4',
        'K': u'кКkK',
        'C': u'цЦcC',
        'R': u'рРpPrR',
        'H': u'хХxXhH',
        'YI': u'йЙy',
        'YA': u'яЯ',
        'YO': u'ёЁ',
        'YU': u'юЮ',
        'B': u'бБ6bB',
        'T': u'тТtT',
        'HS': u'ъЪ',
        'SS': u'ьЬ',
        'Y': u'ыЫ'
    }

    exceptions = [
            u'команд',
            u'рубл',
            u'премь',
            u'оскорб',
            u'краснояр',
            u'бояр',
            u'ноябр',
            u'карьер',
            u'мандат',
            u'употр',
            u'плох',
            u'интер',
            u'веер',
            u'фаер',
            u'феер',
            u'hyundai',
            u'тату',
            u'браконь',
            u'roup',
            u'сараф',
            u'держ',
            u'слаб',
            u'ридер',
            u'истреб',
            u'потреб',
            u'коридор',
            u'sound',
            u'дерг',
            u'подоб',
            u'коррид',
            u'дубл',
            u'курьер',
            u'экст',
            u'try',
            u'enter',
            u'oun',
            u'aube',
            u'ibarg',
            u'16',
            u'kres',
            u'глуб',
            u'ebay',
            u'eeb',
            u'shuy',
            u'ансам',
            u'cayenne',
            u'ain',
            u'oin',
            u'тряс',
            u'ubu',
            u'uen',
            u'uip',
            u'oup',
            u'кораб',
            u'боеп',
            u'деепр',
            u'хульс',
            u'een',
            u'ee6',
            u'ein',
            u'сугуб',
            u'карб',
            u'гроб',
            u'лить',
            u'рсук',
            u'влюб',
            u'хулио',
            u'ляп',
            u'граб',
            u'ибог',
            u'вело',
            u'ебэ',
            u'перв',
            u'eep',
            u'ying',
            u'laun',
            u'чаепитие',
    ]

    patterns = ['\w*[{P}][{I}{E}][{Z}][{D}]\w*',  # пизда
                '(?:[^{I}{U}\s]+|{N}{I})?(?<!стра)[{H}][{U}][{YI}{E}{YA}{YO}{I}{L}{YU}\'](?!иг)\w*', # хуй; не пускает "подстрахуй", "хулиган"
                '\w*[{B}][{L}](?:[{YA}]+[{D}{T}]?|[{I}]+[{D}{T}]+|[{I}]+[{A}]+)(?!х)\w*',  # бля, блядь; не пускает "бляха"
                '(?:\w*[{YI}{U}{E}{A}{O}{HS}{SS}{Y}{YA}][{E}{YO}{YA}{I}][{B}{P}](?!ы\b|ол)\w*)',  # не пускает "еёбы", "наиболее", "наибольшее"...
                '[{E}{YO}][{B}]\w*|[{I}][{B}][{A}]\w+|[{YI}][{O}][{B}{P}]\w*',  # ебать
                '\w*[{S}][{C}]?[{U}]+(?:[{CH}]*[{K}]+|[{CH}]+[{K}]*)[{A}{O}]\w*',  # сука
                '\w*(?:[{P}][{I}{E}][{D}][{A}{O}{E}]?[{R}](?!о)\w*',  # не пускает "Педро"
                '[{P}][{E}][{D}][{E}{I}]?[{G}{K}])',  # пидарас
                '\w*[{Z}][{A}{O}][{L}][{U}][{P}]\w*',  # залупа
                '\w*[{M}][{A}][{N}][{D}][{A}{O}]\w*',  # манда
        ]

    def is_allowed(self, text, charset='UTF-8'):
        original = text
        self.filter_text(text, charset)
        return original == text

    def filter_text(self, text, charset='UTF-8'):
        utf8 = 'UTF-8'
        if charset != utf8:
            text.encode(charset)

        text = text.decode(charset)

        regexp = '\d*(' + '|'.join(self.patterns) + ')'
        regexp = regexp.decode('utf-8').format(**self.letters)

        obscene_words = re.findall(regexp, text)
        i = 0
        while len(obscene_words) - i:
            is_exception = False
            for exception in self.exceptions:
                if obscene_words[i].lower().find(exception) != -1:
                    del obscene_words[i]
                    is_exception = True
                    break

            if not is_exception:
                print obscene_words[i]
                print text.find(obscene_words[i])
                for symb in (' ', ',', ';', '.', '!', '-', '?', "\t", "\n"):
                    obscene_words[i].replace(symb, '')
                text = text.replace(obscene_words[i], '*'*len(obscene_words[i]))
            i += 1
        return text

